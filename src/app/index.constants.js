/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('labexercise')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
