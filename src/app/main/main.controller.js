(function() {
  'use strict';

  angular
    .module('labexercise')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, $interval) {
    var vm = this;

    var slides = [
      'url(../assets/images/banner01.jpg)',
      'url(../assets/images/banner02.jpg)',
      'url(../assets/images/banner03.jpg)'
    ];

    vm.slide = slides[0];
    $interval(function () {
      if(vm.slide == slides[0]){
        vm.slide = slides[1];
      }
      else if(vm.slide == slides[1]) {
        vm.slide = slides[2];
      }
      else if(vm.slide == slides[2]) {
        vm.slide = slides[0];
      }
    }, 3000, 0);
  }
})();
